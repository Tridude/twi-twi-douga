var video_overlay;
var canvas;
function setupOverlay() {
    video_overlay_parent = $("#player");
    video_overlay = video_overlay_parent.find(".player-overlay.player-fullscreen-overlay.js-control-fullscreen-overlay");
    video_overlay.append("<canvas id=\"vid-canvas\"></canvas>");
    canvas = video_overlay.find("#vid-canvas");
    canvas = new fabric.Canvas(canvas[0]);
}

function resizeCanvas() {
    canvas.setHeight(video_overlay.height());
    canvas.setWidth(video_overlay.width());
    canvas.renderAll();
}

function renderEverything() {
    window.requestAnimationFrame(renderEverything);
    // canvas.renderAll();
    resizeCanvas();
}

function setupChatTrigger() {
    var display_chat = false;
    // checks if a required module is there?
    var view_proto = require("web-client/components/chat-line")["default"].prototype;
    // function that runs whenever there's a new chat meessage
    // runs twice for some reason
    view_proto.didInsertElement = function() {
        // variable this is a view_proto object for each chat message?
        display_chat = !display_chat;
        if (!display_chat) {
            return;
        }        
        console.log("from " + this.get("msgObject.from"));
        console.log("\t " + this.get("msgObject.message"));
        var text = new fabric.Text(this.get("msgObject.message"),
            {left: canvas.width, top:Math.floor((Math.random()*(canvas.height-120))), fill:"white"}
        );
        canvas.add(text);
        text.animate("left", 0, {
                duration:5000+text.width*2, 
                // t: time elapsed, b: start, c: end, d: duration
                easing: function(t, b, c, d) {return canvas.width + text.width - (canvas.width+text.width*2)*(t/d)},
                // easing: function(t, b, c, d) {return c*(t/d)+b},
                onComplete: function() {canvas.remove(text)}
            }
        );
    };
}

var deferred = new $.Deferred();
var promise = deferred.promise();
promise = promise.then(function() {
    return jQuery.getScript("http://cdnjs.cloudflare.com/ajax/libs/fabric.js/1.5.0/fabric.min.js")
});
promise.done(function() {
    setupOverlay();
    window.addEventListener('resize', resizeCanvas, false);
    resizeCanvas();
    renderEverything();
    setupChatTrigger();
});

deferred.resolve();